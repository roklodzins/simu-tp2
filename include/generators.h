/**
 * @file generators.c/h
 * @brief implementation of complexe random functions
 * @copyright Romain KLODZINSKI - ISIMA F2 ZZ2 - (c) 2023
 * */


#ifndef TP2_GENERATORS_H
#define TP2_GENERATORS_H


///////////////////////   Functions   /////////////////////////

/**
 * @brief generate a random number in the specified interval [a, b], following an uniform distribution
 * @param[in] a lower (included) limit of the random number interval
 * @param[in] b upper (included) limit of the random number interval
 * @return the random number in [a, b]
 * @note a must be lower or equal to b
 * */
double uniform(double a, double b);


/**
 * @brief generate a random number using the specified mean, following a negative exponential distribution
 * @param[in] mean the mean of the law
 * @return the random number in [0, inf[
 * */
double negExp(double mean);


/**
 * @brief generate 2 random numbers using the Box & Muller Algo, following the specified gaussian law : N(mean, stddev)
 * @param[out] x1 the first random number (cosine)
 * @param[out] x2 the second random number (sine)
 * @param[in] mean the mean of the gaussian law
 * @param[in] stddev the standard deviation of the gaussian law
 * */
void boxMuller(double * x1, double * x2, double mean, double stddev);


/**
 * @brief generate 1 random number using the Box & Muller Algo, following the specified gaussian law : N(mean, stddev)
 * @param[in] mean the mean of the gaussian law
 * @param[in] stddev the standard deviation of the gaussian law
 * @return the random generated number
 * @note this function caches the second number of a pair, to return it at the following call,
 * seed reset may be delayed by 1 call (impair-call)
 * @note variant of boxMuller
 * */
double boxMullerDirect(double mean, double stddev);


/**
 * @brief generate a random numbers using the rejection Algo, following the specified gaussian law : N(mean, stddev)
 * @param[in] mean the mean of the gaussian law
 * @param[in] stddev the standard deviation of the gaussian law
 * @return the random number
 * @note settings are pre-optimized to following correctly the gaussian density
 * */
double gaussianRejection(double mean, double stddev);


/**
 * @brief generate a random numbers using the rejection Algo, following the specified gaussian law : N(mean, stddev)
 * @param[in] width the semi-width of the x-axis interval for Monte Carlo: [-width, width]
 * @param[in] height the height of the y-axis interval for Monte Carlo: [0, height]
 * @param[in] mean the mean of the gaussian law
 * @param[in] stddev the standard deviation of the gaussian law
 * @return the random number
 * @note configurable variant of gaussianRejection
 * */
double gaussianRejectionParam(double width, double height, double mean, double stddev);


/**
 * @brief compute a random distribution of the given population sample
 * @param[in] iter number of iterations
 * @param[in] classes number of classes (size of indiv)
 * @param[in] indiv number of individuals per class
 * @param[in] total number of individuals (or 0: autocomputed)
 * @return the distribution array or NULL on error (must be freed)
 * */
int * discreteDist(int iter, int classes, const int * indiv, int pop);


#endif //TP2_GENERATORS_H
