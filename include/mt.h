/**
 * @file mt.c/h
 * @brief implementation of the Mersenne Twister
 * @copyright Makoto Matsumoto & Takuji Nishimura
 * */

#ifndef TP2_MT_H
#define TP2_MT_H


///////////////////////   Functions   /////////////////////////

void init_genrand(unsigned long s);
void init_by_array(unsigned long init_key[], int key_length);

unsigned long genrand_int32(void);
long genrand_int31(void);

double genrand_real1(void);
double genrand_real2(void);
double genrand_real3(void);
double genrand_res53(void);


#endif //TP2_MT_H

