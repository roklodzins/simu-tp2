/**
 * @file experiments.c/h
 * @brief implementation of the experiments & tests functions
 * @copyright Romain KLODZINSKI - ISIMA F2 ZZ2 - (c) 2023
 * */


#ifndef TP2_EXPERIMENTS_H
#define TP2_EXPERIMENTS_H


////////////////////////  Includes  ///////////////////////////

#include "experiments.h"
#include "generators.h"
#include "utils.h"
#include "mt.h"
#include <malloc.h>
#include <stdarg.h>
#include <math.h>


///////////////////////   Functions   /////////////////////////

/**
 * @brief perform the test sequence specified by the Question 2 of the subject,
 * testing the uniform function
 * @param[in] iter the number of iteration = the number of numbers to generate
 * @param[in] show print generated numbers to the terminal if non-zero
 * */
void uniformExp(int iter, int show);


/**
 * @brief perform a test sequence specified by the Question 3 of the subject,
 * testing the distribution by classes
 * @param[in] iter the number of iteration = the number of individuals to generate (see iter for discreteDist)
 * @param[in] classes the number of classes (see classes for discreteDist)
 * @param[in] ... the number of individuals per classes (see indiv for discreteDist)
 * */
void classdistExp(int iter, int classes, ...);


/**
 * @brief perform a test sequence specified by the Question 4 of the subject,
 * testing the mean of negExp
 * */
void negMeanExp();


/**
 * @brief perform a test sequence specified by the Question 4 of the subject,
 * testing the distribution of the negExp function
 * */
void negDistExp();


/**
 * @brief perform a test sequence specified by the Question 5 of the subject,
 * testing the distribution of many experiences using dice
 * */
void diceSumExp();


/**
 * @brief perform a test sequence specified by the Question 5 of the subject,
 * formatting & showing the given data
 * @param[in] bins_num the number of bins to use to condensed the data
 * @param[in] dice_sum the number of dice per sum value (see diceSumExp)
 * */
void diceBinsExp(int bins_num, const int * dice_sum);


/**
 * @brief perform a test sequence specified by the Question 5 of the subject,
 * testing the distribution & stats of the boxMuller function
 * */
void boxMullerExp();


/**
 * @brief perform a random search to get the best settings used by the gaussian rejection
 * @param[in] iter the number of iteration = the number of tested settings
 * @note this work by performing a (not very optimized) Monte Carlo search
 * @note the best result found will be printed to the terminal
 * */
void rejectionOptimizeExp(int iter);


/**
 * @brief export to the terminal as CSV format the specified number of rundom numbers
 * @param[in] prompt the prompt to show / RNG function name
 * @param[in] iter the number of iteration = of shown numbers
 * @param[in] mu mean taken by the callback
 * @param[in] sigma standard deviation taken by the callback
 * @param[in] callback the RNG function (assumed to be a Gaussian), call as callback(mu, sigma)
 * */
void csvExport(char * prompt, int iter, double mu, double sigma, double (* callback)(double, double));


#endif //TP2_EXPERIMENTS_H
