/**
 * @file experiments.c/h
 * @brief implementation of the experiments & tests functions
 * @copyright Romain KLODZINSKI - ISIMA F2 ZZ2 - (c) 2023
 * */


////////////////////////  Includes  ///////////////////////////

#include "experiments.h"
#include "generators.h"
#include "utils.h"
#include "mt.h"
#include <malloc.h>
#include <stdarg.h>
#include <math.h>


////////////////////////   Macros   ///////////////////////////

#define DICE_EXP 1000000
#define DICE_BIN 101


///////////////////////   Functions   /////////////////////////

/**
 * @brief perform the test sequence specified by the Question 2 of the subject,
 * testing the uniform function
 * @param[in] iter the number of iteration = the number of numbers to generate
 * @param[in] show print generated numbers to the terminal if non-zero
 * */
void uniformExp(int iter, int show) {
	double mean = 0;
	
	H2("Test of uniform(-89.2, 56.7) with %d iterations\n" RST, iter);
	
    for (int i = 0; i < iter; i++) {
		
		double r = uniform(-89.2, 56.7);
		mean += r;
		
        if (show) {
			if (i % 5 == 0) putchar('\n');
			printf("%10.8f\t", r);
		}
    }
	
	H3("\n\nMean: " RST "%.8f\n\n", mean / iter);
}


/**
 * @brief perform a test sequence specified by the Question 3 of the subject,
 * testing the distribution by classes
 * @param[in] iter the number of iteration = the number of individuals to generate (see iter for discreteDist)
 * @param[in] classes the number of classes (see classes for discreteDist)
 * @param[in] ... the number of individuals per classes (see indiv for discreteDist)
 * */
void classdistExp(int iter, int classes, ...) {
    va_list arg;
    va_start(arg, classes);

	
    int * dist, * indiv = malloc(sizeof (int[classes]));
    int pop = 0;

	
    if (indiv) {
		
		H2("Dist with %d iterations using initial classes = ", iter);
		
        for (int i = 0; i < classes; i++) {
			indiv[i] = va_arg(arg, int); // Load Variadic args
	        pop += indiv[i];             // Get the total indiv. num
	        printf("%c : %d | ", 'A' + i, indiv[i]);
        }
		
        dist = discreteDist(iter, classes, indiv, pop);
		
		if (dist) { // Print Results
	        H3("\n                ");
			
	        for (int i = 0; i < classes; i++) printf("\t%c\t|", 'A' + i);
	        H3("\nDistribution  : ");
			
	        for (int i = 0; i < classes; i++) print("\t%d\t|", dist[i]);
	        H3("\nDistrib (as %%): ");
			
	        for (int i = 0; i < classes; i++) print("\t%.3f\t|", dist[i] * 100. / iter);
			printf("\n\n\n");
			
			free(dist);
			
        } else H1("discreteDist Failed: alloc error\n");

        free(indiv);
		
    } else H1("classdistExp Failed: alloc error\n");

	
    va_end(arg);
}


/**
 * @brief perform a test sequence specified by the Question 4 of the subject,
 * testing the mean of negExp
 * */
void negMeanExp() {
	double mean = 0;
	int max = 1000;
	int i = 0;
	
	do {
		H2("Test negExp mean with %d iterations (theorical mean = 11)\n", max);
		
		for (; i < max; i++) mean += negExp(11);
		
		H3("Mean: " RST "%.8f\n\n", mean / max);
		
		max += 1000000 - 1000;
	} while (max <= 1000000); // personally I prefer a goto statement here...
}


/**
 * @brief perform a test sequence specified by the Question 4 of the subject,
 * testing the distribution of the negExp function
 * */
void negDistExp() {
    int bins[21] = {0};
    int max = 1000, i = 0;
    double mean = 0;

	do {
		H2("\nTest negExp distribution with %d iterations (theorical mean = 10)\n", max);

		/// Fill bins & compute the mean
	    for (; i < max; i++) {
			double rd = negExp(10);
	        int rdn = (int)rd;
			
	        bins[rdn > 20 ? 20: rdn]++; // 20+ in the 21st cell (cell n°20)
	        mean += rd;
	    }
	
		print_gfx(bins, 21, 80, 0);
		
		/// Print Results
	    for (int u = 0; u < 21; u++) {
	        if (u == 20) H3("[20 :\t+∞[\t ");
	        else H3("[%d :\t%d[\t ", u, u + 1);
	        print("%d\t(%.3f%%)\n", bins[u], bins[u] * 100 / (double)max);
	    }

        H3("\nMean: " RST "%.8f\n\n", mean / max);

		max += 1000000 - 1000;
	} while (max <= 1000000);
}


/**
 * @brief perform a test sequence specified by the Question 5 of the subject,
 * testing the distribution of many experiences using dice
 * */
void diceSumExp() {
	
    int i, dice_sum[DICE_BIN] = {0}; // dice_sum[i]: number of experiments ended with sum = i (in [0:100], not [20:120])
    double smean = 0, // square mean
	       mean = 0, variance, stddev;
	
	H2("Dice Sum Experiment repeated %d times\n\n", DICE_EXP);
	
	/// Dice Exp
	
	/// 'many' = DICE_EXP experiments
    for (i = 0; i < DICE_EXP; i++) {
		
		/// an experiment of 20 draws
        int sum = 0;
        for (int u = 0; u < 20; u++) sum += (int)(genrand_int32() % 6); // [0:5]

        dice_sum[sum]++; // count the result
    }
	
	/// Compute mean & mean²
    for (i = 0; i < DICE_BIN; i++) {
        int u = (i + 20) * dice_sum[i]; // +20: slide the interval to [20:120]
        mean  += u;
        smean += u * (i + 20); // value² * occurrence, not u²
    }

	mean  /= DICE_EXP;
    smean /= DICE_EXP;
    variance = smean - mean * mean; // Var(X) = E(X²) - E(X)²
    stddev = sqrt(variance);        // standard deviation

    H3("Mean :\t\t\t "           RST "%.8f\n", mean);
    H3("Variance :\t\t "         RST "%.8f\n", variance);
    H3("Standard Deviation :\t " RST "%.8f\n\n", stddev);
	
	/// Dice Exp Data Formatting
    //int bins_num = (int)(DICE_BIN / stddev) + 1;        // "my feelings v1" formula
    //int bins_num = 1 + (int)log2(DICE_EXP);             // Sturges' Formula
    int bins_num = 1 + (int)(log2(DICE_EXP * variance));  //"my feelings v2" formula
	
	diceBinsExp(bins_num, dice_sum);
	
	int input = 0;
	Ask(input, "\nPrint the (beautiful) graphical repr. of the original 101 bins ? (0 = no | 1 = yes | 2 = yes using log scale) : ");
	
	if (input) print_gfx(dice_sum, DICE_BIN, 80, input == 2);
}


/**
 * @brief perform a test sequence specified by the Question 5 of the subject,
 * formatting & showing the given data
 * @param[in] bins_num the number of bins to use to condensed the data
 * @param[in] dice_sum the number of dice per sum value (see diceSumExp)
 * */
void diceBinsExp(int bins_num, const int * dice_sum) {
	
    int i, * bins = calloc(bins_num, sizeof(int));

    if (bins) {
		
		/// Dispatch the 101 default-bins into "bins_num" bins
        for (i = 0; i < DICE_BIN; i++) {
            int dst = (int)(i * bins_num / DICE_BIN);
            bins[dst] += dice_sum[i];
        }
		
		H3("Graphical representation of the %d bins (log scale)\n", bins_num);
	    print_gfx(bins, bins_num, 80, 1);
		
		/// Print Results
		for (int u = 0; u < bins_num; u++) {
	        H3("[%d :\t%d[\t " RST "%d\t(%.3f%%)\n",
			   20 + u * DICE_BIN / bins_num, 20 + (u + 1) * DICE_BIN / bins_num,
			   bins[u], bins[u] * 100. / DICE_EXP);
		}
		
        free(bins);
		
    } else H1("diceBinsExp Failed : alloc error\n\n");
}


/**
 * @brief perform a test sequence specified by the Question 5 of the subject,
 * testing the distribution & stats of the boxMuller function
 * */
void boxMullerExp() {
	int bins[20] = {0}; // Box Muller bins
	int max = 1000, i = 0;
	double mean = 0, smean = 0, variance, stddev;
	
	do {
		H2("\n\nTest boxMuller distribution with %d iterations\n", max);
		
		/// Generate random numbers
		for (; i < max >> 1; i++) { // 2 num per iter -> max / 2
			double r1, r2;
			boxMuller(&r1, &r2, 0, 1);
			
			if (r1 >= -1 && r1 <= 1) bins[(int) ((r1 + 1) * 10)]++; // translate [-1 ; 1] to [0 ; 2] to [0 ; 20]
			if (r2 >= -1 && r2 <= 1) bins[(int) ((r2 + 1) * 10)]++; // translate [-1 ; 1] to [0 ; 2] to [0 ; 20]
			
			mean += r1 + r2;
			smean += r1 * r1 + r2 * r2;
		}
		
		mean /= max;
		smean /= max;
		variance = smean - mean * mean; // Var(X) = E(X²) - E(X)²
		stddev = sqrt(variance);        // standard deviation
		
		H3("Mean :\t\t\t "           RST "%.8f\n", mean);
		H3("Variance :\t\t "         RST "%.8f\n", variance);
		H3("Standard Deviation :\t " RST "%.8f\n\n", stddev);
		
		print_gfx(bins, 20, 80, 0);
		
		/// Print Results
		for (int u = 0; u < 20; u++) {
			H3("[%.1f :\t%.1f%c\t " RST "%d\t(%.3f%%)\n",
			   u / 10. - 1, (u + 1) / 10. - 1, u == 19 ? ']' : '[',
			   bins[u], bins[u] * 100. / max);
		}
		
		max += 1000000 - 1000;
	} while (max <= 1000000);
}


/**
 * @brief perform the sub-experiment of testing the gaussian rejection with the specified settings
 * @param[in] iter the number of iteration = the sample size
 * @param[in] width the 'width' setting passed to gaussianRejectionParam
 * @param[in] height the 'height' setting passed to gaussianRejectionParam
 * @param[out] o_mean the output for the computed mean, of the sample
 * @param[out] o_stddev the output for the computed standard deviation, of the sample
 * */
static inline void rejectionOptimizeSubExp(int iter, double width, double height, double * o_mean, double * o_stddev) {

	register double mean = 0, smean = 0;
	
		for (int i = 0; i < iter; i++) {
			register double r = gaussianRejectionParam(width, height, 0, 1);
			
			mean += r;
			smean += r * r;
		}
		
		mean /= iter;
		smean /= iter;
		*o_mean = mean;
		*o_stddev = sqrt(smean - mean * mean);
}


/**
 * @brief perform a random search to get the best settings used by the gaussian rejection
 * @param[in] iter the number of iteration = the number of tested settings
 * @note this work by performing a (not very optimized) Monte Carlo search
 * @note the best result found will be printed to the terminal
 * */
void rejectionOptimizeExp(int iter) { // iter=50000, lost seed?: w=4.854806 h=0.451888 : m=-0.000022 s=1.000011
	double w, h, mean, stddev;
	double best_mean = INFINITY, best_stddev = INFINITY;
	double best_w = INFINITY, best_h = INFINITY;

    H3("\nRejection optimization...\n\n           ");
    for (int i = 0; i < iter / 100; i++) putchar('_');
    print("\nProgress : ");
    fflush(stdout);
	
	for (int i = 0; i < iter; i++){
		w = genrand_real1() * 5.; // [-5 ; 5]   may be optimized, using a dichotomy like approch maybe
		h = genrand_real1() / 2.; // [0 ; 0.5]  may be optimized, using a dichotomy like approch maybe

        rejectionOptimizeSubExp(1000000, w, h, &mean, &stddev);

        if (i % 100 == 0) {
            putchar('|');
            fflush(stdout);
        }

        /// Keep as best if both mean & stddev are better (can be improved?)
		if (fabs(mean) < fabs(best_mean) && fabs(stddev - 1) < fabs(best_stddev - 1)) {
            best_stddev = stddev, best_mean = mean,
            best_w = w, best_h = h;
        }
	}

    H2("\n\nGaussian Rejection - Best Settings :\n");
    H3("Width : \t " RST "%.8f\n", best_w);
    H3("Height :\t " RST "%.8f\n\n",  best_h);
    H3("Mean :\t\t\t " RST "%.8f\n",  best_mean);
    H3("Standard Deviation :\t " RST "%.8f\n\n", best_stddev);
}


/**
 * @brief export to the terminal as CSV format the specified number of rundom numbers
 * @param[in] prompt the prompt to show / RNG function name
 * @param[in] iter the number of iteration = of shown numbers
 * @param[in] mu mean taken by the callback
 * @param[in] sigma standard deviation taken by the callback
 * @param[in] callback the RNG function (assumed to be a Gaussian), call as callback(mu, sigma)
 * */
void csvExport(char * prompt, int iter, double mu, double sigma, double (* callback)(double, double)) {
	int input = 0;
	Ask(input, "\nPrint %d random numbers (as csv for Excel) following the gaussian law N(%d, %d) using %s ? (0 = no | 1 = yes) : ",
	   iter, (int)mu, (int)sigma, prompt);
		
	if (input) {
		double mean = 0, smean = 0, variance, stddev;
		
		for (int i = 0; i < iter; i++) {
			if (i % 100 == 0) putchar('\n');
			
			double r = callback(mu, sigma);
			
			printf("%.8f;", r);
			mean += r;
			smean += r * r;
	    }
	
		mean /= iter;
	    smean /= iter;
	    variance = smean - mean * mean; // Var(X) = E(X²) - E(X)²
	    stddev = sqrt(variance);        // standard deviation
		
		H3("\n\nMean :\t\t\t "           RST "%.8f\n", mean);
		H3("Variance :\t\t "         RST "%.8f\n", variance);
		H3("Standard Deviation :\t " RST "%.8f\n\n", stddev);
	}
}
