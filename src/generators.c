/**
 * @file generators.c/h
 * @brief implementation of complexe random functions
 * @copyright Romain KLODZINSKI - ISIMA F2 ZZ2 - (c) 2023
 * */


////////////////////////  Includes  ///////////////////////////

#include "generators.h"
#include "mt.h"
#include <math.h>
#include <malloc.h>


///////////////////////   Functions   /////////////////////////

/**
 * @brief generate a random number in the specified interval [a, b], following an uniform distribution
 * @param[in] a lower (included) limit of the random number interval
 * @param[in] b upper (included) limit of the random number interval
 * @return the random number in [a, b]
 * @note a must be lower or equal to b
 * */
double uniform(double a, double b) {
    return a + genrand_real1() * (b - a);
}


/**
 * @brief generate a random number using the specified mean, following a negative exponential distribution
 * @param[in] mean the mean of the law
 * @return the random number in [0, inf[
 * */
double negExp(double mean) {
    return -mean * log(1 - genrand_real2()); // use the [0, 1[ variant to avoid log(0)
}


/**
 * @brief generate 2 random numbers using the Box & Muller Algo, following the specified gaussian law : N(mean, stddev)
 * @param[out] x1 the first random number (cosine)
 * @param[out] x2 the second random number (sine)
 * @param[in] mean the mean of the gaussian law
 * @param[in] stddev the standard deviation of the gaussian law
 * */
void boxMuller(double * x1, double * x2, double mean, double stddev){
	double a = 6.283185307179586 * genrand_real1(),
	       b = stddev * sqrt(-2 * log(1 - genrand_real2())); // (1 - gen2) <=> gen]0, 1] = log(0) safe
	*x1 = cos(a) * b + mean;
	*x2 = sin(a) * b + mean;
}


/**
 * @brief generate 1 random number using the Box & Muller Algo, following the specified gaussian law : N(mean, stddev)
 * @param[in] mean the mean of the gaussian law
 * @param[in] stddev the standard deviation of the gaussian law
 * @return the random generated number
 * @note this function caches the second number of a pair, to return it at the following call,
 * seed reset may be delayed by 1 call (impair-call)
 * @note variant of boxMuller
 * */
double boxMullerDirect(double mean, double stddev) {
	static double r1, r2;
	static int cache_level = 0;
	
	if (!cache_level) {
		boxMuller(&r1, &r2, mean, stddev);
		cache_level = 1;
	} else cache_level = 0;
	
	return cache_level ? r1: r2;
}


/**
 * @brief generate a random numbers using the rejection Algo, following the specified gaussian law : N(mean, stddev)
 * @param[in] mean the mean of the gaussian law
 * @param[in] stddev the standard deviation of the gaussian law
 * @return the random number
 * @note settings are pre-optimized to following correctly the gaussian density
 * */
double gaussianRejection(double mean, double stddev){
	double x, y;
	
	do {
		x = uniform(-3.93913276, 3.93913276);   // Get from rejectionOptimizeExp
		y = uniform(0, 0.45129310);             // Get from rejectionOptimizeExp
	} while (y > exp(-x * x / 2.) / sqrt(6.283185307179586));
	
	return x * stddev + mean;
}


/**
 * @brief generate a random numbers using the rejection Algo, following the specified gaussian law : N(mean, stddev)
 * @param[in] width the semi-width of the x-axis interval for Monte Carlo: [-width, width]
 * @param[in] height the height of the y-axis interval for Monte Carlo: [0, height]
 * @param[in] mean the mean of the gaussian law
 * @param[in] stddev the standard deviation of the gaussian law
 * @return the random number
 * @note configurable variant of gaussianRejection
 * */
double gaussianRejectionParam(double width, double height, double mean, double stddev){
	double x, y;
	
	do {
		x = uniform(-width, width); // centred symmetry of the curve
		y = uniform(0, height);     // 0 : positivity of the curve
	} while (y > exp(-x * x / 2.) / sqrt(6.283185307179586));
	
	return x * stddev + mean;
}


/**
 * @brief compute a random distribution of the given population sample
 * @param[in] iter number of iterations
 * @param[in] classes number of classes (size of indiv)
 * @param[in] indiv number of individuals per class
 * @param[in] total number of individuals (or 0: autocomputed)
 * @return the distribution array or NULL on error (must be freed)
 * */
int * discreteDist(int iter, int classes, const int * indiv, int pop) {

    double * cumulated_proba = malloc(sizeof (double[classes - 1])); // classes - 1: last cumul proba is assumed to be 1
    int * dist = NULL;

    if (cumulated_proba) {

        dist = calloc(classes, sizeof(int));

        if (dist) {
			
			if (!pop) for (int i = 0; i <  classes; i++) pop += indiv[i]; // compute pop if not provided

			/// Compute cumulated proba
            cumulated_proba[0] = indiv[0] / (double) pop;

            for (int i = 1; i < classes - 1; i++) {  // classes - 1: last cumul proba is assumed to be 1
                cumulated_proba[i] = cumulated_proba[i - 1] + indiv[i] / (double) pop;
            }

			/// Generate random individuals
            for (int cls, i = 0; i < iter; i++) {
                double proba = genrand_real1();

                for (cls = 0; cls < classes - 1 && proba >= cumulated_proba[cls]; cls++); // else: cls = last class

                dist[cls]++;
            }
        }
        free(cumulated_proba);
    }

    return dist;
}
