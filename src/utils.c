/**
 * @file utils.c/h
 * @brief implementation of utilities functions
 * @copyright Romain KLODZINSKI - ISIMA F2 ZZ2 - (c) 2023
 * */

#include <math.h>
#include "utils.h"


/**
 * @brief print a graphical representation of the provided distribution
 * @param[in] dist the distribution
 * @param[in] size the distribution size
 * @param[in] width the max width in character, printed in the terminal
 * @param[in] log_scale show the repr. using the log scale (non-zero = enable)
 * */
void print_gfx(const int * dist, int size, int width, int log_scale) {
	int max = 0, ul;
	for (int i = 0; i < size; i++) if (max < dist[i]) max = dist[i];
	
	for (int i = 0; i < size; i++) {
		
        print("\n| ");
        set_bg(200 - 145 * i / size, 0, 55 + 145 * i / size); // pretty colorful result :-)
        set_fg(200 - 145 * i / size, 0, 55 + 145 * i / size);
		
		if (log_scale)
			ul = (int) (log(1 + dist[i]) * width / log(1 + max));
		else
			ul = dist[i] * width / max;
		
        for (int u = 0; u < ul; u++) putchar('|');
	}
	
	print("\n\n");
}
