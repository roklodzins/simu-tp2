/**
 * @file main.c
 * @brief Simulation TP2 - F2 ZZ2
 * @copyright Romain KLODZINSKI - ISIMA F2 ZZ2 - (c) 2023
 * */


////////////////////////  Includes  ///////////////////////////

#include <stdio.h>
#include "experiments.h"
#include "mt.h"
#include "utils.h"
#include "generators.h"


////////////////////////    Main    ///////////////////////////

/**
 * @brief the main program, Simulation TP2 - F2 ZZ2
 * */
int main(void) {
	int input = 0;
    unsigned long init[4] = {0x123, 0x234, 0x345, 0x456};
    init_by_array(init, 4); // MT init

    H1("Simulation F2 ZZ2 - TP2 : Romain KLODZINSKI (c) 2023\n\n\n");


    ////////////////////////  Question 1  ///////////////////////////

	H1("Question 1 - Matsumoto Test : see the end\n\n\n");
	
	
	////////////////////////  Question 2  ///////////////////////////
	
    H1("Question 2 - Generation of uniform random numbers between A and B\n\n");
    
	uniformExp(1000, 1);
	
    Ask(input, "Test of uniform(-89.2, 56.7) with 1000000 iterations ? (0 = no | 1 = yes) : ");
	
	if (input) uniformExp(1000000, 0);
	
	
	////////////////////////  Question 3  ///////////////////////////
	
	H1("\nQuestion 3 - Reproduction of discrete empirical distributions\n\n");
	
    classdistExp(1000, 3, 500, 150, 350);

    classdistExp(1000000, 3, 500, 150, 350);

    classdistExp(1000, 6, 500, 150, 350, 70, 1, 2000);

    classdistExp(3071, 6, 500, 150, 350, 70, 1, 2000);

    classdistExp(1000000, 6, 500, 150, 350, 70, 1, 2000);

	
	////////////////////////  Question 4  ///////////////////////////

    H1("Question 4 - Reproduction of continuous distributions\n\n");
 
	negMeanExp();
	
	negDistExp();
	
	
	////////////////////////  Question 5  ///////////////////////////

    H1("\nQuestion 5 - Simulating non reversible distribution laws\n\n");
	
	diceSumExp();
	
	boxMullerExp();
	
	csvExport("BoxMuller", 10000, 10, 3, boxMullerDirect);
	
	csvExport("rejection algo", 50000, 10, 3, gaussianRejection);

    Ask(input, "Start the Gaussian Rejection Optimization (2000 iterations) ? (0 = no | 1 = yes) : ");

	if (input) rejectionOptimizeExp(2000);


	////////////////////////  Question 1  ///////////////////////////

    H1("\nQuestion 1 : Matsumoto test\n\n" RST);
	init_by_array(init, 4); // MT init
	
	int i;
	
	printf("1000 outputs of genrand_int32()\n");
    for (i = 0; i < 1000; i++) {
        printf("%10lu ", genrand_int32());
        if (i % 5 == 4) printf("\n");
    }
    printf("\n1000 outputs of genrand_real2()\n");
    for (i = 0; i < 1000; i++) {
		printf("%10.8f ", genrand_real2());
		if (i % 5 == 4) printf("\n");
    }
	
    return 0;
}
